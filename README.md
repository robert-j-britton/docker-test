[![CircleCI](https://circleci.com/bb/robert-j-britton/docker-test.svg?style=svg)](https://circleci.com/bb/robert-j-britton/docker-test)

# Available Scripts (dev)

In the project directory, you can run:


### `docker build -f Dockerfile.dev .`

Builds a Docker development image.

### `docker run -p 3000:3000 <IMAGE_ID>`

Spin up a Docker image with no mappings. Changes to the project are not reflected in the Docker container until the image is rebuilt and container re-run.

### `docker run -p 3000:3000 -v /app/node_modules -v <absolute_path_to_project_root_pwd>:/app <IMAGE_ID>`

Spin up a Docker container mapping local project volumes with container to allow for React's hot reloading feature.

### `docker-compose up`

Builda a docker image of the project and runs a container. Uses the configuration found in the docker-compose.yml located in the project root.

### `docker run -it <IMAGE_ID> npm run test`

Run tests in interactive mode.

### `docker run -p 3000:3000 -v /app/node_modules -v <absolute_path_to_project_root_pwd>:/app -it <IMAGE_ID> npm run test npm run test`

Run tests interactively, local changes are reflected inside the container.


### `docker exec -it <CONTAINER_ID> npm run test`

Alternatively you can run tests on an existing container. (Piggybacking off an existing container)


# Available Scripts (prod)

In the project directory, you can run:

### `docker build .`

Build the Docker image from the default Dockerfile configuration.

### `docker run -p 8080:80 <IMAGE_ID>`

Run the container with the production image.


## [Site Preview](http://dockerreact-env.auargp2s4x.eu-west-2.elasticbeanstalk.com/)
